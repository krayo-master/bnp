package sk.krayo.bnp.mappers;

import org.springframework.stereotype.Component;
import sk.krayo.bnp.domain.Customer;
import sk.krayo.bnp.dto.CustomerDto;

@Component
public class CustomerPatchMapper {

    @SuppressWarnings("OptionalAssignedToNull")
    public Customer mapCustomerPatch(Customer customer, CustomerDto updatedCustomer) {
        if (updatedCustomer.getFirstName() != null) {
            customer.setFirstName(updatedCustomer.getFirstName().orElse(null));
        }
        if (updatedCustomer.getFirstName() != null) {
            customer.setFirstName(updatedCustomer.getFirstName().orElse(null));
        }
        if (updatedCustomer.getLastName() != null) {
            customer.setLastName(updatedCustomer.getLastName().orElse(null));
        }
        if (updatedCustomer.getMiddleName() != null) {
            customer.setMiddleName(updatedCustomer.getMiddleName().orElse(null));
        }
        if (updatedCustomer.getEmail() != null) {
            customer.setEmail(updatedCustomer.getEmail().orElse(null));
        }
        if (updatedCustomer.getPhoneNumber() != null) {
            customer.setPhoneNumber(updatedCustomer.getPhoneNumber().orElse(null));
        }
        if (updatedCustomer.getBirthDate() != null) {
            customer.setBirthDate(updatedCustomer.getBirthDate().orElse(null));
        }
        return customer;
    }

}
