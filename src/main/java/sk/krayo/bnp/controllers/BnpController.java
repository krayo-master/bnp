package sk.krayo.bnp.controllers;

import org.springframework.web.bind.annotation.*;
import sk.krayo.bnp.domain.Customer;
import sk.krayo.bnp.domain.Quotation;
import sk.krayo.bnp.domain.Subscription;
import sk.krayo.bnp.dto.CustomerDto;
import sk.krayo.bnp.errors.ResourceNotFoundException;
import sk.krayo.bnp.mappers.CustomerPatchMapper;
import sk.krayo.bnp.repositories.CustomerRepository;
import sk.krayo.bnp.repositories.QuotationRepository;
import sk.krayo.bnp.repositories.SubscriptionRepository;

import java.util.Optional;

@RestController
public class BnpController {

    private final CustomerRepository customerRepository;
    private final QuotationRepository quotationRepository;
    private final SubscriptionRepository subscriptionRepository;
    private final CustomerPatchMapper customerPatchMapper;

    public BnpController(CustomerRepository customerRepository, QuotationRepository quotationRepository,
                         SubscriptionRepository subscriptionRepository, CustomerPatchMapper customerPatchMapper) {
        this.customerRepository = customerRepository;
        this.quotationRepository = quotationRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.customerPatchMapper = customerPatchMapper;
    }

    @PostMapping("/quotations")
    public Quotation createQuotation(@RequestBody Quotation newQuotation) {
        return quotationRepository.save(newQuotation);
    }

    @PostMapping("/subscriptions")
    public Subscription createSubscription(@RequestBody Subscription newSubscription) {
        return subscriptionRepository.save(newSubscription);
    }

    @GetMapping("/subscriptions/{id}")
    public Subscription retrieveSubscription(@PathVariable Long id) {
        return subscriptionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Subscription with id: " + id + " not found."));
    }

    @PutMapping("/customers/{id}")
    public Customer replaceCustomer(@PathVariable Long id, @RequestBody Customer replacedCustomer) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            replacedCustomer.setId(customerOptional.get().getId());
            return customerRepository.save(replacedCustomer);
        }
        throw new ResourceNotFoundException("Customer with id: " + id + " not found");
    }

    @PatchMapping("/customers/{id}")
    public Customer updateCustomer(@PathVariable Long id, @RequestBody CustomerDto updatedCustomer) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            return customerRepository.save(customerPatchMapper.mapCustomerPatch(customerOptional.get(), updatedCustomer));
        }
        throw new ResourceNotFoundException("Customer with id: " + id + " not found");
    }

}
