package sk.krayo.bnp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.krayo.bnp.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
