package sk.krayo.bnp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.krayo.bnp.domain.Quotation;

public interface QuotationRepository extends JpaRepository<Quotation, Long> {
}
