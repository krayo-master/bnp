package sk.krayo.bnp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.krayo.bnp.domain.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
}
