package sk.krayo.bnp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;
import java.util.Optional;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class CustomerDto {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> middleName;
    private Optional<String> email;
    private Optional<String> phoneNumber;
    private Optional<Date> birthDate;
}
