package sk.krayo.bnp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quotation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private Date beginningOfInsurance;
    private Long insuredAmount;
    private Date dateOfSigningMortgage;
    @OneToOne(cascade = CascadeType.ALL)
    private Customer customer;
}
