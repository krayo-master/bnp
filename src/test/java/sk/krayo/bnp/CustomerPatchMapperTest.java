package sk.krayo.bnp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import sk.krayo.bnp.domain.Customer;
import sk.krayo.bnp.dto.CustomerDto;
import sk.krayo.bnp.mappers.CustomerPatchMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CustomerPatchMapperTest {

    @InjectMocks
    private CustomerPatchMapper customerPatchMapper;

    @Test
    void testCustomerPatchMapper_withNullValues() {
        CustomerDto updatedCustomer = new CustomerDto();
        updatedCustomer.setEmail(Optional.empty());
        updatedCustomer.setFirstName(Optional.of("John"));
        updatedCustomer.setLastName(Optional.of("Mcclain"));

        Customer dbCustomer = new Customer();
        dbCustomer.setId(1L);
        dbCustomer.setEmail("johnie@email.com");
        dbCustomer.setFirstName("Frank");
        dbCustomer.setLastName("Sinatra");

        Customer dbCustomerAfterUpdate = customerPatchMapper.mapCustomerPatch(dbCustomer, updatedCustomer);
        assertThat(dbCustomerAfterUpdate.getEmail()).isEqualTo(null);
        assertThat(dbCustomerAfterUpdate.getFirstName()).isEqualTo("John");
        assertThat(dbCustomerAfterUpdate.getLastName()).isEqualTo("Mcclain");
    }

}
