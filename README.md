# bnp

## Build application:
mvnw clean install
## Run application:
mvnw spring-boot:run

## Notes:
Wasn't entirely sure about the expected behaviour of "Endpoint for updating Customer should be able to update and/or remove existing values of any/all attributes." so i created both PUT (replace entire entry) and PATCH endpoint (replace/remove specific attributes) where behaviour of null (since this wasnt defined) became a problem that has multiple possible solutions. I went with Optional attributes in DTO as i think its pretty straight forward solution even though its not very clean.

## Improvements:
This wasnt really considered for such a small excercise but if we were to expand the project we could use the following:
- separate controllers per entity
- service layer 
- DTOs
